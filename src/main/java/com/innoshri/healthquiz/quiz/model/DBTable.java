package com.innoshri.healthquiz.quiz.model;

public class DBTable {
	   private String tableName;
	    private String columnName;
	    private String dataType;
	    private String constraint_type;
	    private int ordinalPosition;
		public String getTableName() {
			return tableName;
		}
		public void setTableName(String tableName) {
			this.tableName = tableName;
		}
		public String getColumnName() {
			return columnName;
		}
		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}
		public String getDataType() {
			return dataType;
		}
		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
		public String getConstraint_type() {
			return constraint_type;
		}
		public void setConstraint_type(String constraint_type) {
			this.constraint_type = constraint_type;
		}
		public int getOrdinalPosition() {
			return ordinalPosition;
		}
		public void setOrdinalPosition(int ordinalPosition) {
			this.ordinalPosition = ordinalPosition;
		}
	    
}
