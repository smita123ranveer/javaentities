package com.innoshri.healthquiz.quiz.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.innoshri.healthquiz.quiz.HttpResponse.ErrorResponse;
import com.innoshri.healthquiz.quiz.HttpResponse.HttpResponse;
import com.innoshri.healthquiz.quiz.HttpResponse.UploadFileResponse;
import com.innoshri.healthquiz.quiz.service.ClassFileService;
import com.itextpdf.text.DocumentException;

@RequestMapping(path = "/rest") // This means URL's start with /demo (after Application path)
@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private ClassFileService classService;

    @GetMapping("/table/{tableName}")
    
    public String uploadFile(@PathVariable String tableName) {
        String message = "files will be created for "+ tableName;
        message = classService.createClasses(tableName);

        return message;
        		
     }

       
    
}
