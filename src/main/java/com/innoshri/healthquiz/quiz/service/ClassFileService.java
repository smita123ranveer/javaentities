package com.innoshri.healthquiz.quiz.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.innoshri.healthquiz.quiz.config.FileStorageProperties;
import com.innoshri.healthquiz.quiz.exception.FileStorageException;
import com.innoshri.healthquiz.quiz.exception.MyFileNotFoundException;
import com.innoshri.healthquiz.quiz.model.DBTable;
import com.itextpdf.text.DocumentException;


@Service
public class ClassFileService {

	private static Environment env;
	@PersistenceContext
	private EntityManager entityManager;
	

    public List<DBTable> getTableDetails(String tableName) {
		// TODO Auto-generated method stub
    	String query = "SELECT cols.column_name,cols.data_type,usg.ordinal_position, cnst.constraint_type\r\n" + 
    			"FROM INFORMATION_SCHEMA.COLUMNS cols \r\n" + 
    			"Left join information_schema.key_column_usage AS usg\r\n" + 
    			"on cols.table_name = usg.table_name and cols.column_name=usg.column_name\r\n" + 
    			"LEFT JOIN information_schema.table_constraints AS cnst\r\n" + 
    			"ON cnst.constraint_name = usg.constraint_name\r\n" + 
    			"WHERE cols.table_name = :tableName;";
    	  // Query jpqlQuery = entityManager.createQuery("SELECT u FROM UserEntity u WHERE u.id=:id");
    	   Query jpqlQuery = entityManager.createQuery(query);
    	    jpqlQuery.setParameter("tableName", tableName);
    	    return (List<DBTable>) jpqlQuery.getResultList();
    	

	}

	public String createClasses(String tableName) {
		// TODO Auto-generated method stub
		List<DBTable> columns = getTableDetails(tableName);
		if (columns != null)
		{
			return columns.size()+ " columns found for " + tableName;
		}
		else
		{
			return "No Data found for " + tableName;
		}
	}
		   

}
