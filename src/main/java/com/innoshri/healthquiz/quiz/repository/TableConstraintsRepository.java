//package com.innoshri.healthquiz.quiz.repository;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//
//import com.innoshri.healthquiz.quiz.model.TableConstraints;
//
//
//// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//// CRUD refers Create, Read, Update, Delete
//@Repository
//@Transactional
//public interface TableConstraintsRepository extends JpaRepository<TableConstraints, Long>,JpaSpecificationExecutor<TableConstraints> {
//
//	@Query(value = "select * from tbl_user user where user.username=:usernameOrEmail or user.username=:usernameOrEmail2 ", nativeQuery = true)
//	public Optional<TableConstraints> findByUsernameOrEmail(String usernameOrEmail, String usernameOrEmail2);
//
//
//	
//
//}
