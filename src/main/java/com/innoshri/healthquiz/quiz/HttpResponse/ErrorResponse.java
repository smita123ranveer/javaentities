package com.innoshri.healthquiz.quiz.HttpResponse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "responseStatus", "responseMessage", "response" })
public class ErrorResponse extends HttpResponse {

	public ErrorResponse(Integer responseStatus, Object responseMessage, Object response) {
		super(responseStatus, responseMessage, response);
		// TODO Auto-generated constructor stub
	}

	public ErrorResponse(String message, Exception e) {
		this.setResponseStatus(500);
		this.setResponseMessage(message);
		this.setErrorMessage(e.getMessage());
		
		// TODO Auto-generated constructor stub
	}

}