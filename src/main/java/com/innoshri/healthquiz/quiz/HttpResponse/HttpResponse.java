package com.innoshri.healthquiz.quiz.HttpResponse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "responseStatus", "responseMessage", "response" })
public class HttpResponse {

	@JsonProperty("responseStatus")
	private Integer ResponseStatus;
	@JsonProperty("responseMessage")
	private Object ResponseMessage;
	@JsonProperty("errorMessage")
	private Object ErrorMessage;

	@JsonProperty("nextAction")
	private Object NextAction;

	@JsonProperty("userId")
	private Object userId;

	@JsonProperty("response")
	private Object Response;
	@JsonProperty("responseArray")
	private List<Object> ResponseArray;

	public HttpResponse()
	{
		super();
	}
	

	public HttpResponse(Integer responseStatus, Object responseMessage, Object response) {
		super();
		ResponseStatus = responseStatus;
		ResponseMessage = responseMessage;
		Response = response;
	}

	public Integer getResponseStatus() {
		return ResponseStatus;
	}

	public void setResponseStatus(Integer responseStatus) {
		ResponseStatus = responseStatus;
	}

	public Object getResponseMessage() {
		return ResponseMessage;
	}

	public void setResponseMessage(Object responseMessage) {
		ResponseMessage = responseMessage;
	}

	public Object getResponse() {
		return Response;
	}

	public void setResponse(Object response) {
		Response = response;
	}

	public Object getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(Object errorMessage) {
		ErrorMessage = errorMessage;
	}

	public Object getNextAction() {
		return NextAction;
	}

	public void setNextAction(Object nextAction) {
		NextAction = nextAction;
	}

	public List<Object> getResponseArray() {
		return ResponseArray;
	}

	public void setResponseArray(List<Object> responseArray) {
		ResponseArray = responseArray;
	}


	public Object getUserId() {
		return userId;
	}


	public void setUserId(Object userId) {
		this.userId = userId;
	}

}
