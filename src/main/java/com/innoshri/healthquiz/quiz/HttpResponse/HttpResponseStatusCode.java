package com.innoshri.healthquiz.quiz.HttpResponse;

public class HttpResponseStatusCode {

	public static final int RESPONSE_OK = 200;
	public static final int RESPONSE_FAILED = 101;
	public static final int RESPONSE_REGISTER_FAILED = 201;
	public static final int RESPONSE_REGISTER_USER_ALREADYEXIST = 202;

	public static final int RESPONSE_LOGIN_USER_INACTIVE = 203;
	public static final int RESPONSE_LOGIN_UNIQUE_CODE = 205;
	public static final int RESPONSE_LOGIN_EMPTY_UNIQUE_CODE = 204;
	public static final int RESPONSE_LOGIN_FAILED_CREDENTIALS = 206;
	public static final int RESPONSE_LOGIN_USER_DOES_NOT_EXIST = 210;
	public static final int RESPONSE_LOGIN_USER_AND_CODE_NOT_MATCHES = 211;

	public static final int RESPONSE_USER_LIST = 212;
	public static final int RESPONSE_USER_LIST_EMPTY = 213;

	public static final int RESPONSE_PARTY_SUCCESS = 214;

}
